'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _mysql2 = require('mysql2');

var _mysql22 = _interopRequireDefault(_mysql2);

var pool = _mysql22['default'].createPool({
    host: 'localhost',
    user: 'root',
    password: 'nfhbrmysql',
    database: 'fightzone'
});

var query = function query(sql, props) {
    pool.getConnection(function (error, connection) {
        connection.query(sql, props, function (error, response) {
            return response;
        });
        connection.release();
    });
};

exports['default'] = query;
module.exports = exports['default'];
//# sourceMappingURL=db.js.map