'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _db = require('./db');

var _db2 = _interopRequireDefault(_db);

var app = (0, _express2['default'])();

app.use(_bodyParser2['default'].urlencoded({ extended: false }));
app.use(_bodyParser2['default'].json());

app.get('*', function (req, res) {
    (0, _db2['default'])('SELECT * from category', function (err, rows) {
        if (err) res.send(err);else res.json(rows);
    });
});

app.listen(3000, function () {
    console.log('Server listening on port 3000');
});
//# sourceMappingURL=index.js.map