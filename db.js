import mysql from 'mysql2'

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'nfhbrmysql',
    database: 'fightzone'
});

const query = (sql, props) => {
    pool.getConnection((error, connection) => {
        connection.query(sql, props, (error, response) => {
                return response
            }
        );
        connection.release()
    })
};

export default query