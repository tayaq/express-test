import express from 'express'
import bodyParser from 'body-parser'
import query from './db'
const app = express();


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('*', (req, res) => {
    query('SELECT * from category', (err, rows) => {
        if(err) res.send(err)
        else res.json(rows);
    })
});

app.listen(3000, () => {
    console.log('Server listening on port 3000');
});
